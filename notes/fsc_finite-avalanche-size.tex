\documentclass[aps, prx, twocolumn]{revtex4-1}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usepackage{color}
\usepackage{siunitx}
\sisetup{mode=text,range-phrase = -}


\begin{document}
\title{Finite-size correction with finite avalanche size}
\author{Anna, Jens and Johannes}
%

\begin{abstract}
We can correct for finite-size convergence effects formally by introducing a
branching parameter as a function of network activity. However, this introduces
a diverging branching parameter for fully active networks. This divergence
causes activation with probability 1, leading to never ending avalanches. While
infinite avalanches are theoretically the correct result, they are not
practicable and for a finite lattice meaningless in terms of information
processing. Here, we show the minimal requirement on the divergent branching
parameter to ensure avalanches of finite lifetime. Altogether, the resulting
finite-size corrected model shows critical behavior up to the maximal
avalanche-size encodable by the network size, but with a well defined behavior
above. We focus here on $m^\ast=1$ in the regime of separation of timescales
(STS) without external input.
\end{abstract}
\maketitle


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Preliminaries}
The following problems are formulated for a fully connected branching network
with (activity-dependent) branching parameter $m=m(A)$. The probability to
activate another node (including self-activation) is $p=m(A)/N$. For $m(A)>N$,
activity will cover the full network and does never die out again. While this
appears somewhat special, we will see that this case is encountered in the
finite-size corrected branching network. In this case, the fully connected
constraint is important, because drawing (correct would be binomially distributed) $k$
connections leads to $p=m(A)/k$, which saturates much earlier but the number of
connections does not increase. This could be recovered if one increases the
number of connections with activity level, which is numerically not better than
fully connected. However, fixing the number of connections avoids infinite
avalanches in the first place, but introduces subtleties which we were not sure
how to control.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Finite-size convergence}
\label{secConvergence}
We begin by recovering the finite-size convergence effects. Due to multiple
activation of the same neuron, the effective branching parameter as a function
of network activity $A_t$ is 
%
\begin{equation}\label{eqBNfs}
  m_N(A_t)=\left(\frac{N}{A_t}\right)\left(1-\left(1-{\frac{m}{N}}\right)^{A_t}\right).
\end{equation}
%
The maximal convergence is obviously achieved for $A_t=N$, where $m_N(N)$
describes the fraction of neurons excited on average despite all neurons are
active in the previous time step. In the limit we find 
%
\begin{equation}\label{eqBNconvergenceLimit}
   \lim_{N\to\infty} m_N(N)
  = 1- \lim_{N\to\infty} \left(1-\frac{m}{N}\right)^N
  = 1-e^{-m}.
\end{equation}
%
This limit is quite interesting, because it directly give a lower bound on the
convergence effects. For $m=1$ we obtain $m_N(N)\to 1-e^{-1}\approx 0.632$.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
  \includegraphics{figures/fig_STS_fsc_m-divergence.pdf}
  \caption{%
    Finite-size correcting branching parameter $m(A)$. For $m^\ast\geq 1$ this
    diverges formally at $A_d=N/m^\ast$ as $m(A)\to\infty$. For $m^\ast<1$ but
    $m^\ast\to1$, $m(N)\to N$ which numerically introduces apparent never-ending
    avalanches. 
    \label{figFscDivergence}
  }
\end{figure}
\section{Finite-size correction and the divergence issue}
We can correct the finite-size convergence effects by introducing a
network-activity dependent branching parameter $m(A)$, such that for each
activity level the effective branching parameter satisfies a target value, i.e.,
$m_N(A)=m^\ast$. Hence,
%
\begin{equation}\label{eqBNfsc}
  m(A) = N\left(1-\left(1-\frac{m^\ast A}{N}\right)^{1/A}\right)
\end{equation}

The finite-size correcting branching parameter $m(A)$ diverges for $m^\ast\geq
1$ at $A_d=N/m^\ast$, since the argument of the root $(\cdot)^{1/A}$ goes to
zero. However, also for $m^\ast<1$ but $m^\ast\to1$, $m(N)\to N$. While this is
not divergent, we will see below that the probability to stop a network-spanning
avalanche vanishes. Figure~\ref{figFscDivergence} shows $m(A)$ for different
$m^\ast$. In the following, we focus on $m^\ast\leq1$ when we consider full network
activity. The results should be straight-forward for $m^\ast>1$ if treated at
$A_d$.

An important limit we will need below, is the behavior of $m(N-1)$ for
$m^\ast\leq1$ in the limit $N\to\infty$. First, we notice that the case
$m^\ast=1$ is an upper bound and only consider this. Then, 
\begin{align}\label{eqLimitFscNminus1}
  m(N-1)&\leq N\left(1-\left(1-\frac{N-1}{N}\right)^\frac{1}{N-1}\right)\nonumber\\
        &=N\left(1-\left(\frac{1}{N}\right)^\frac{1}{N-1}\right)\nonumber\\
        &=N\left(1-e^{\frac{1}{N-1}\ln(1/N)}\right)\nonumber\\
        &\simeq N\left(1-1-\frac{1}{N-1}\ln(1/N)\right)\nonumber\\
        &\simeq\ln(N)
\end{align}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Probability to stop a network-spanning avalanche}
Let us now calculate the probability to stop an avalanche, which spans the full
network, i.e., $A_t=N$. This corresponds to the probability that at least 1 site
is not activated due to convergence effects. This already hints at a connection
to Sec.~\ref{secConvergence}. Moreover, we will see that this is related to the
problem of disconnected Erd\H{o}s-R{\'e}nyi (ER) graphs if the probability of
adding an edge is identified with the probability of activating another neuron. 

The probability for neuron $i$ to activate neuron $j$ is $p_{ij}=m(N)/N$ and
consequently, the probability that neuron $i$ is not activated by $j$ is
$\bar{p}_{ij}=1-m(N)/N$. Thus, the probability to activate neuron $i$ by all $N$
neurons in the network is $P_i=1-\bar{p}_{ij}^N = 1-(1-m(N)/N)^N$. The
probability to activate all neurons in the network, given that all neurons were
already active in the previous step, is then $P=(1-(1-m(N)/N)^N)^N$. Our first
result is thus the probability to {\it not} activate at least 1 neuron:
%
\begin{equation}\label{eqPnotONE}
  \bar{P} = 1-P = 1-\left(1-\left(1-\frac{m(N)}{N}\right)^N\right)^N.
\end{equation}
%
This corresponds to the probability of stopping a network-spanning avalanche as
long as $m(N-1)$ provides a possibility of non-divergent avalanche size. To
check this, we assume that \eqref{eqPnotONE} is an upper bound estimate using
$m^\ast=1$ and $m(N-1)\simeq\ln(N)$ instead. [To properly do this, one would now
need to redo the above steps for $NA=N-1$, where however one imposes at least
two neurons to be not activated, ...]. We find since $\ln(N)\ll N$ for
$N\to\infty$ and using $\ln(1-x)\simeq -x$
\begin{align}\label{eqPnotONENminusONE}
  \bar{P}(N-1, m^\ast=1) 
  &\geq 1-\left(1-\left(1-\frac{\ln(N)}{N}\right)^N\right)^N \nonumber\\
  &=1-e^{N\ln(1-e^{N\ln(1-\ln(N)/N)})} \nonumber\\
  &\simeq1-e^{N\ln(1-e^{-\ln(N)})} \nonumber\\
  &=1-e^{N\ln(1-1/N)} \nonumber\\
  &\simeq1-e^{-1}
\end{align}

The result \eqref{eqPnotONENminusONE} shows that the probability to not activate
a subset of neurons is larger than a non-zero value for $m^\ast\leq 1$ if $A<N$.
At the same time \eqref{eqPnotONENminusONE} tells us that $m(N)=\ln(N)$ is
already a suitable choice to circumvent runaway avalanches. However, in the
following we will derive this result explicitly.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Avoiding runaway avalanches}
We ask the question, how to choose $m(N)$ for $m^\ast\leq 1$ such that once the
network is fully active there is a route back to less activity and eventually
finishing avalanches. To motivate the problem, let us consider an intuitive but
wrong choice, namely $m(N)=cN$ with $c<1$, such that $p_{ij}=c<1$. Then, the
probability to stop an avalanche in the limit $N\to\infty$ becomes
%
\begin{align}
  \bar{P} &= 1-\left(1-\left(1-c\right)^N\right)^N\nonumber\\
   &= 1-e^{-N(1-c)^N} \to0
\end{align}
since $\lim_{N\to\infty} N(1-c)^N=\lim_{N\to\infty} Ne^{N\ln(1-c)}=0$ as for
$1-c<1$ we get $\ln(1-c)<0$ such that the exponential function decays much
faster to zero than $N$ diverges.

Now, let us invert \eqref{eqPnotONE} to find a solution for the branching
parameter at full network activity
%
\begin{align}\label{eqAnsatzCutoff}
  m(N) 
  &= N\left(1-\left(1-\left(1-\bar{P}\right)^{1/N}\right)^{1/N}\right)\nonumber\\
  &= N\left(1-\left(1-e^{a/N}\right)^{1/N}\right),
\end{align}
%
where we have introduced $a=\ln(1-\bar{P})<0$. Next, we aim for an asymptotic
expansion around $N\to\infty$ of the inner function. For large $N$ we can expand
the exponential function $e^{a/N}= 1+a/N+(a/N)^2/2+\dots$ such that 
%
\begin{align}\label{eqAsymptoticExpansion}
  \left(1-e^{a/N}\right)^{1/N}
  &= \left(1-1-a/N-(a/N)^2/2+\dots\right)^{1/N}\nonumber\\
  &= (-a/N)^{1/N}\left[1-(a/N)/2+\dots\right]^{1/N}
\end{align}
%
Restricting ourselves to the leading order we get 
\begin{align}\label{eqAnsatzSolution}
  m(N) 
  &\simeq N\left(1-\left(-a/N\right)^{1/N}\right)  
  =       N\left(1-e^{\frac{1}{N}\ln(-a/N)}\right) \nonumber\\
  &\simeq N\left(1-1-\frac{1}{N}\ln(-a/N)\right)   \nonumber\\
  &=      \ln\left(\frac{N}{-\ln(1-\bar{P})}\right). 
\end{align}
%
Here, we ensured that $\lim_{N\to\infty}\frac{1}{N}\ln(-a/N)=0$ according to
l'Hospital rule and expanded $e^{x}\simeq 1+x$.

Let us briefly discuss some special cases: 
\begin{itemize}
  \item For $\bar{P}=+0$ no avalanche that reaches full network size is ever
  stopped. In this case, $-\ln(1-\bar{P})\to 0$ such that $m(N)\to\infty$ for
  finite $N$. However, this still renders the problem that $\bar{P}\to 1$ for
  $N\to\infty$ if $m(N)$ chosen not as \eqref{eqAnsatzSolution}
  
  \item For $\bar{P}\to1$ at least 1 neuron is not activated in the fully active
  network, i.e., all all infinite-avalanches are hindered. Here,
  $-\ln(1-\bar{P})\to\infty$ and $m(N)\to-\infty$ for finite $N$ which does not
  make much sense. Still, it is easily possible to choose inappropriate $m(N)$
  to find $\bar{P}\to1$ in the limit $N\to\infty$.

  \item For $\bar{P}=1-e^{-1}$, we recover the most simple form $m(N)=\ln(N)$.

  \item For a finite $N$, one has to keep in mind that $m(N)$ is upper bounded
  by convergence effects. Hence, not any choice of $\bar{P}$ is suitable for a
  finite $N$. Our most secure choice so far was $m(N)=\ln(N)$. 
\end{itemize}

\begin{figure}
  \includegraphics{figures/fig_STS_fsc_prob-stop-avalanche.pdf}
  \caption{%
    Probability to stop an infinite avalanche in the limit of $N\to\infty$ for
    different choices of $m(N)$. 
    \label{figFscProbStopAvalanche}
  }
\end{figure}


\subsection{Interpretation in terms of ER networks}
This can be readily understood if interpreted in terms of disconnected ER
graphs. The active neurons are the nodes of the graph. If neuron $i$ activates
neuron $j$, then this establishes an edge $(i,j)$. If now the full graph is
connected, then chances are high that each node received at least an in-going
edge (corresponding to being activated). On the other hand, if the graph is
disconnected, then chances are high that at least one node did not receive an
in-going connection.

The threshold probability to create an edge $p_{\rm edge}=\ln(N)/N$ determines
whether the graph is disconnected $p<p_{\rm edge}$ or connected $p>p_{\rm
edge}$. Since for finite systems each node of a sparsely connected graph has a
finite probability of having only out degrees, $p_{\rm edge}$ is a lower bound
to ensure stopping infinite-size avalanches. Since we identified this with the
probability to activate a neuron, we obtain $m(N)=p_{\rm edge}N=\ln(N)$ as
result. Due to the simplicity of the arguments, this may be a good approach to
treat different network topologies besides all-to-all interacting networks.

\section{open things}
\begin{itemize}
  \item connection between \eqref{eqBNconvergenceLimit} and $\hat{P}$ for
  $m(N)=\ln(N)$ or \eqref{eqPnotONENminusONE}. Minimum number of activated
  neurons due to convergence is $1-e^{-1}$ for $m=1$ and probability for
  saturating avalanche is also $1-e^{-1}$ if we choose $m(N)=m(N-1)$

  \item generalizations ... 
\end{itemize}

\end{document}
