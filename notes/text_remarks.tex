\documentclass[aps, prx, reprint]{revtex4-1}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usepackage{color}
\usepackage{siunitx}
\sisetup{mode=text,range-phrase = -}


\begin{document}
\title{Finite-size corrections in branching networks due to convergence effects}
\author{Anna Levina$^{1,2}$}
\author{Jens Wilting$^{3}$}
\author{Johannes Zierenberg$^{2,3}$}
\author{Viola Priesemann$^{2,3}$}
\affiliation{
  $^1$ Tuebingen \newline
  $^2$ Bernstein Center for Computational Neuroscience, Am Fassberg 17, 37077 G{\"o}ttingen, Germany,
  $^3$ Max Planck Institute for Dynamics and Self-Organization, Am Fassberg 17, 37077 G{\"o}ttingen, Germany
}
%

\begin{abstract}
\end{abstract}
\maketitle

We calculate the finite-size corrections in a branching network, defined as the
realization of a branching process on a graph of finite size $N$. The finite
size facilitates convergence, i.e., a neuron gets activated by two or more
neurons. This reduces the average amount of neurons activated in the next time
step, or in other words the (finite-size) branching parameter $m_N$. Here, we
consider the annealed disorder average where connections between neurons are
redrawn in each step with probability $p_{\rm s}=k/N$ such that there are on
average $k$ connections per neuron. This is mathematically equivalent to a
fully-connected network and different from the quenched average considered in
Ref.~\cite{kinouchi2006}. The probability to activate a connected neuron is
$p_{\rm a}=m/k$ such that on average $m$ new neurons are activated by an active
neuron, besides convergence effects. In addition we consider an external
stimulus per neuron, modelled by a Poisson process with rate $h$ resulting in
activation probability $\lambda(h)=1-\exp(-h\Delta t)$. The probability that a
given neuron $i$ is activated is complicated to compute, however the probability
that neuron $i$ is not activated by neuron $j$ is straight forward: $p_{\rm na}
= (1 - p_{\rm s}) + p_{\rm s}(1-p_{\rm a}) = 1-m/N$. Thus, the probability of
activating neuron $i$ 
%
\begin{equation}\label{eqProbActivationDriven}
  p^i(A_t|N,m)=1-\left(1-\frac{m}{N}\right)^{A_t}\left(1-\lambda(h)\right)=p(A_t)
\end{equation}
%
This considers that we have a pool of active neurons which can activate neuron
$i$ in a fully connected network, opposed to the quenched average where a pool
of connections are considered~\cite{kinouchi2006}. Then, the probability that
$A_{t+1}$ neurons are activated in time step $t+1$ given that $A_t$ neurons are
active at time $t$ is expressed by the binomial distribution
%
%\begin{equation}\label{eqActivity}
$  P(A_{t+1} | A_t,N,m) = \binom{N}{A_{t+1}}
  p(A_t)^{A_{t+1}}\left(1-p(A_t)\right)^{N-A_{t+1}}$,
%\end{equation}
%
with expectation value $E[A_{t+1}] = Np(A_t)$. Introducing the finite-size
branching parameter $m_N$ such that $E[A_{t+1}]=m_N A_t + \lambda(h)N$ allows us
to identify
%
\begin{equation}\label{eqBNfs}
  m_N(A_t)=
  \left(\frac{N}{A_t}\right)\left(1-\left(1-{\frac{m}{N}}\right)^{A_t}\right)\left(1-\lambda(h)\right),
\end{equation}
%
where $m$ is the model (or hidden) branching parameter and $m_N$ is the
effective branching parameter seen in the time series.

We can calculate the probability distribution $P(A)$ via the transition matrix
$T_{ij} = P(A_{t+1}=j | A_t=i)$ that transforms a distribution $\pi_i T =
\pi_{i+1}$, as stationary solution $TP(A)=P(A)$. This allows us to analytically
predict the finite-size corrections for different definitions of the effective
finite-size branching parameter $m_N$:
\begin{itemize}
  \item $m_N^1 = \int m_N(A)P(A)dA$
  \item $m_N^2 = \int m_N(A) A P(A)dA/\int A P(A)dA$
  \item $m_N^3 = Cov[A_{t+1}, A_t]/Var[A_t, A_t]$
\end{itemize}
Only the latter is accessible in experiments~\cite{wilting2016}. Commonly one
uses $m^4=\langle\frac{A_{t+1}}{A_t}\rangle$ which we will show does not
reproduce the finite-size corrections, neither the infinite-size limit well.
Instead, it overestimates the branching parameter in presence of external input.
See Fig.~\ref{figFss}. We define the finite-size scaling limit as the limit
$N\rightarrow\infty$ with $h=const.$, i.e., fixed per neuron input. This
could be realized with optogenetic stimulation. Different definitions could
include local input, e.g., from electrodes which could be described by fixed
network activity $\langle A\rangle$. This may seems physiologically plausible
but is indeed not so easy to define in mean-field calulcations.

\begin{figure}
  \includegraphics[width=\columnwidth]{./figures_goal/fig_fss.png}
  \includegraphics[width=\columnwidth]{./figures_goal/fig_fss_h.png}
  \caption{%
    Finite-size scaling of finite-size network branching parameter. Deviations
    from the infinite-size parameter are due to convergence effects, which
    diminish in the limit of large networks. (top) Different definitions of the
    finite-size estimated branching parameter converge to the same value, while
    the standard estimate does not. (bottom) Different external input strength
    converge to the same rescaled limit, if $h$ is sufficiently small.
    \label{figFss}
  }
\end{figure}

We can, however, exploit \eqref{eqBNfs} to construct a branching network which
corrects for finite-size effects by imposing $m_N(A_t)=\hat{m}$, where $\hat{m}$
is the desired branching parameter. The solution provides us with the
activity-dependent finite-size corrected model parameter 
%
\begin{equation}\label{eqBNfsc}
  m(A_t) = N\left(1-\left(1-\frac{\hat{m} A_t}{N(1-\lambda(h))}\right)^{1/A_t}\right)
\end{equation}

\vspace{1em}
We can calculate the dynamic range for our annealed network by solving
%
\begin{equation}
  A = N(1-(1-m/N)^A(1-\lambda(h)))
\end{equation}
%
After some proper rearrangement, we can identify the lambert-W function $W(z)
e^{W(z)}=z$, yielding  
\begin{equation}
  A(h) = N - \frac{W\left[(1-m/N)^N\ln(1-m/N)N(1-\lambda(h))\right]}{\ln(1-m/N)}
\end{equation}
and
\begin{equation}
  h(A) = \frac{1}{\Delta t}\ln\left(\frac{1-A/N}{(1-m/N)^A}\right)
\end{equation}
%
The dynamic range is then defined as
$\Delta=10\log_{10}\left(h_{0.9}/h_{0.1}\right)$,where $h_x$ refers to the rate
at which the response $A_x=A(h_x)=A_0+x(A_{\rm max}-A_0)$ with $A_0=A(0)$ and
$A_{\rm max}$ the minimal and maximal response, respectively.

\begin{figure*}
  \includegraphics[width=\columnwidth]{./figures_goal/fig_fsc_avalanche-distribution.png}
  \includegraphics[width=\columnwidth]{./figures/fig_STS_avalanche-size.pdf}
  \includegraphics[width=\columnwidth]{./figures_goal/fig_fsc_avalanche-distribution_h.png}
  \includegraphics[width=\columnwidth]{./figures/fig_driven_avalanche-size_h.pdf}
  \caption{%
    LEFT:
    Avalanche-size distribution in a finite-size branching network. The
    finite-size correction scheme \eqref{eqFSC} reproduces the expected
    infinite-size distribution over several orders of magnitude. This holds for
    critical and subcritical $m$ (top) as well as for different external input
    $h$ (bottom).\newline
    RIGHT:
    (top) crosses are finite-size networks
    ($N=10^2,10^3,10^4$) and boxes is a $N=100$ network with finite-size
    corrections. (bottom) finite-size corrected networks for different external
    input. Once the input becomes too large, the definition of avalanches is
    shaky. 
    \label{figFscAvalanch}
  }
\end{figure*}

\begin{figure}
  \caption{%
  }
\end{figure}


\begin{figure}
  \includegraphics[width=\columnwidth]{./figures_goal/fig_fsc_dynamic-range.png}
  \caption{%
    Dynamic range for finite network of size $N=100$. The peak is shifted as in
    Fig.~\ref{figFss}? Potentially finite-size scaling leads to increase of
    dynamic range in the vicinity of the peak where mean-field solution fails?
    \label{figDynamicRange}
  }
\end{figure}



\section{First Results}

We measured avalanche-size distributions of branching networks both in the
driven ($h\neq0$) and separation-of-timescales regime (STS, $h\rightarrow0$). We
performed simulations until we gained $10^6$ avalanches. For each distribution,
we repeated this $12$ times to estimate the statistical error. There are a few
technical issues we noted:
%
\begin{itemize}
  \item In the network each node may activate itself
  \item The random connections are drawn without replacement, which causes a
    slight bump in the avalanche-size distributions of the finite-size corrected
    version
  \item the correction factor $m(A)$ diverges for $A\rightarrow N$. Thus, we
    consider for now $m(N)=m(N-1)$, which explains the exponential decay of the
    distribution at large avalanches. This is ensured in our implementation also
    because of the random sampling of connections, whereas for $k=N$, we could
    set $m(N)=N$ and the network would never cease activity. This is in
    principle correct because an infinite system should experience avalanches on
    all scales, including infinity. Obviously, this does not make sense
    numerically. 
\end{itemize}


\begin{thebibliography}{10}
  \bibitem{kinouchi2006}%dynamic range
    O. Kinouchi and M. Copelli,
    {\it Optimal dynamical range of excitable networks at criticality}
    Nat. Phys. {\bf 2}, 348 (2006).
  \bibitem{wilting2016}
    J. Wilting and V. Priesemann,
    {\it Branching into the Unknown: Inferring collective dynamical states from
    subsampled systems},
    arXiv:1608.07035.
\end{thebibliography}
\end{document}

%For our model system of size $N=10^4$ with average activity
%$\avg{A\target}=NR\target\Dt=1$ we would assume to be able to make decent measurements
%until bursts of size $A_t<100$. 
%To assess the branching parameter, we compare several methods: In our
%simulations we have access to the number of recurrently activated nodes
%$A_{t+1}^{\rm rec}$ neglecting the ones activated by the independent input. Per
%definition, the branching parameter is then best estimated by 
%\newcommand{\mREC}{\hat{m}_{\rm rec}}
%%
%\begin{align}\label{eqRECestimator}
%  \mREC = \frac{\avg{A_{t+1}^{\rm rec}}}{\avg{A_t}},
%\end{align}
%%
%which is however subject to internal information of the network, commonly not
%accessible. 
%% MR estimator
%In addition, we make use of a linear regression~\cite{??,wilting2016} which
%remains suitable for experimental measurements of individual spikes, subject to
%subsampling~\cite{wilting2016}:
%\newcommand{\mMR}{\hat{m}_{\rm MR}}
%%
%\begin{align}\label{eqMRestimator}
%  \left(\mMR\right)^k \propto \frac{{\rm Cov}\left[A_{t+k},
%  A_t\right]}{{\rm Var}\left[A_t,A_t\right]},
%\end{align}
%%
%where $\mMR$ is obtained from a fit to \eqref{eqMRestimator}. For the mean-field
%network we show the corresponding estimates of the distance to criticality $1-m$
%as a function of independent input $h$ in Fig.~\ref{figDistCritMF}, where we
%show each show a measurement with initial state quiescent ($m\init=0$,
%$s_{i,0}=0$) and critical ($m\init=1$, $s_{i,0}=1$). We note that for
%$h/R\target\Dt>10^{-2}$ or $1-m>10^{-2}$ both estimators work suitably well.
%Getting closer to criticality leads to increasing population activity and causes
%drastic finite-size effects for the chosen neural population size $N=10^4$ which
%increases deviations in $\mMR$. These deviations are due to the already
%mentioned finite-size effects. We can see that for our network size deviations
%start to occur for temporal activity $A_t\geq200$, which corresponds to $2\%$ of
%the network activity. Still, linear regression is the only decent chance in
%estimating the branching ratio, because other standard approaches such as the
%direct average over the activation in the next time bin
%$\sigma=\avg{A_{t+1}/A_t}$ require separation of time scales, i.e., no
%spontaneous input. Overall, we see that the numerical results converge to the
%predicted behavior and that the multi-regression estimator is able to assess the
%branching parameter well.
