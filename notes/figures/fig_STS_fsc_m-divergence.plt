#
set term epslatex size 8.6cm,6cm standalone color solid 8
set output "fig_STS_fsc_m-divergence.tex"

set xlabel '$A$'
set ylabel '$m(A)$'

set format x '$10^{%T}$'
set logscale x

set key top left

N=100
f(A,m)=N*(1-(1-m*A/N)**(1/A))

set xrange [1:101]

plot \
f(x,0.9) title '\phantom{m}$m^\ast=0.9$',\
f(x,1.0) title '\phantom{m}$m^\ast=1.0$',\
f(x,2.0) title '\phantom{m}$m^\ast=2.0$'


#pause -1

