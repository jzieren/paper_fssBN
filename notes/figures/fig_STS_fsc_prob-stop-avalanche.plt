#
set term epslatex size 8.6cm,6cm standalone color solid 8
set output "fig_STS_fsc_prob-stop-avalanche.tex"

set xlabel '$N$'
set ylabel '$\bar{P}$'

set format x '$10^{%T}$'
set logscale x

set key top right Left 

p(N,m)=1-(1-(1-m/N)**N)**N

set xrange [1:1e7]

plot \
p(x,0.5*log(x)) title '\makebox[6em][l]{$m(N)=\ln(N)/2$}' ls 1,\
p(x,log(x))     title '\makebox[6em][l]{$m(N)=\ln(N)$  }' ls 2,\
p(x,log(2*x))   title '\makebox[6em][l]{$m(N)=\ln(2N)$ }' ls 3,\
p(x,2*log(x))   title '\makebox[6em][l]{$m(N)=\ln(N)2$ }' ls 4,\
1-exp(-1)        notitle dt 2 lc 2,\
1-exp(-1/2.)     notitle dt 2 lc 3,\


#pause -1

