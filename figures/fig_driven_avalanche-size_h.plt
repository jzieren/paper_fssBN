#
set term epslatex size 8.6cm,6cm standalone color solid 8
set output "fig_driven_avalanche-size_h.tex"

set xlabel '$s$'
set ylabel '$P(s)$'

set format xy '$10^{%T}$'
set logscale xy

set key top right
set key spacing 1.5

#set xrange [5e-7:10]
set yrange [1e-11:]

plot \
x**(-3./2.) title '$s^{-3/2}$' lw 2 lc 'black' ,\
"../data/avalanche-size-distributions/result_BN_STS_fsc_avg_distribution_avalanche_full_size_m1.00e+00_N1e+02.dat" u 1:2:3 w e title 'STS'  ls 1,\
"../data/avalanche-size-distributions/result_BN_driven_fsc_avg_distribution_avalanche_full_size_m1.00e+00_h1.00e-06_N1e+02_avalanches.dat" u 1:2:3 w e title '$h=10^{-6}$'  ls 2,\
"../data/avalanche-size-distributions/result_BN_driven_fsc_avg_distribution_avalanche_full_size_m1.00e+00_h1.00e-05_N1e+02_avalanches.dat" u 1:2:3 w e title '$\phantom{h=}10^{-5}$'  ls 3,\
"../data/avalanche-size-distributions/result_BN_driven_fsc_avg_distribution_avalanche_full_size_m1.00e+00_h1.00e-04_N1e+02_avalanches.dat" u 1:2:3 w e title '$\phantom{h=}10^{-4}$'  ls 4,\
"../data/avalanche-size-distributions/result_BN_driven_fsc_avg_distribution_avalanche_full_size_m1.00e+00_h1.00e-03_N1e+02_avalanches.dat" u 1:2:3 w e title '$\phantom{h=}10^{-3}$'  ls 5,\

#pause -1

