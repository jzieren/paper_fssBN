#
set term epslatex size 8.6cm,6cm standalone color solid 8
set output "fig_STS_avalanche-size.tex"

set xlabel '$s$'
set ylabel '$P(s)$'

set format xy '$10^{%T}$'
set logscale xy

set key top right
set key spacing 1.5

#set xrange [5e-7:10]
set yrange [1e-11:]

plot \
x**(-3./2.) title '$s^{-3/2}$' lw 2 lc 'black' ,\
-1                                                                                                                         w l title '$m=1.0$' lc 1,\
-1                                                                                                                         w l title '$m=0.9$' lc 2,\
"../data/avalanche-size-distributions/result_BN_STS_avg_distribution_avalanche_full_size_m1.00e+00_N1e+02.dat"     u 1:2:3 w e notitle       lc 1 lt 2,\
"../data/avalanche-size-distributions/result_BN_STS_avg_distribution_avalanche_full_size_m1.00e+00_N1e+03.dat"     u 1:2:3 w e notitle       lc 1 lt 2,\
"../data/avalanche-size-distributions/result_BN_STS_avg_distribution_avalanche_full_size_m1.00e+00_N1e+04.dat"     u 1:2:3 w e notitle       lc 1 lt 2,\
"../data/avalanche-size-distributions/result_BN_STS_fsc_avg_distribution_avalanche_full_size_m1.00e+00_N1e+02.dat" u 1:2:3 w e notitle       lc 1 lt 4,\
"../data/avalanche-size-distributions/result_BN_STS_avg_distribution_avalanche_full_size_m9.00e-01_N1e+02.dat"     u 1:2:3 w e notitle       lc 2 lt 2,\
"../data/avalanche-size-distributions/result_BN_STS_fsc_avg_distribution_avalanche_full_size_m9.00e-01_N1e+02.dat" u 1:2:3 w e notitle       lc 2 lt 4

#pause -1

