
.PHONY: all figures

all: figures short.pdf long.pdf

FORCE:

figures: 
	$(MAKE) -C figures all

short.pdf: short.tex
				pdflatex short.tex

long.pdf: long.tex
				pdflatex long.tex

clean:
				rm *.log
				rm *.aux
				rm *.bib
