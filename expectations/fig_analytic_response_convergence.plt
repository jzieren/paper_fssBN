#
set terminal epslatex size 8.7cm,6.0cm standalone color colortext 8 header \
   "\\usepackage{amsmath}\n \
    \\newcommand{\\ft}[0]{\\footnotesize}"
set output "fig_analytic_response_convergence.tex"

set xlabel '$h$'
set ylabel '$\langle a\rangle$'

set format x '$10^{%T}$'
set logscale x

#set key top left Left width 4
#set key spacing 1.5
unset key
set border 3
set xtics nomirror
set ytics nomirror 0, 0.5, 1

set xrange [1e-6:10]
set yrange [0:1.1]

set sample 1000

#mean-field solution for N->infty
dt=1
lambda(h)=1-exp(-h*dt)
# important: N*ln(1-m/N)~-m
a(h,m)=1 -lambertw(exp(-m)*(1-lambda(h))*(-m))/(-m)
a_cut(h,m)=a(h,m)<1 ? a(h,m):1


#inverse: solve a(h,m)=0.1 -> lamda=(1-m)*0.1 -> h=-ln(1-(1-m)*0.1)/dt
ax(m,f)=a(1e-15,m) + f*(1-a(1e-15,m))
ha(a,m)=-log(-(a-1)*exp(a*m))/dt
hmin(m)=ha(ax(m,0.1),m)  
hmax(m)=ha(ax(m,0.9),m)  

e="1e0 1e-1 1e-2 1e-3 0 -0.2"
e(i)=word(e, i)
color(i)=word("1 2 3 4 7 8",i)
size=words(e)

# label the curves
#set label 1 sprintf('\ft $m=1-10^{ 0}$') at 9.9e-1,0.8 rotate by 73 center tc ls color(1)
set label 2 sprintf('\ft $m=1-10^{-1}$') at 1.0e-0,0.7 rotate by 65 center tc ls color(2)
#set label 3 sprintf('\ft $m=1-10^{-2}$') at 1.0e-1,0.2 rotate by 50 center tc ls color(3)
#set label 4 sprintf('\ft $m=1-10^{-3}$') at 5.0e-4,0.8 rotate by 81 center tc ls color(4)
set label 5 sprintf('\ft $m=1$'  ) at 7.0e-3,0.17 rotate by 30 center tc ls color(5)
set label 6 sprintf('\ft $m=1.2$') at 5.0e-5,0.35 rotate by  0 center tc ls color(6)

dyn(i)=word("1.0 1.02 1.04 1.06 1.08 1.1",i)+0.

set label 100 "dynamic range" at screen 0.02, graph 1

plot \
for [i=2:size] a_cut(x,1-e(i)) lw 2 lc color(i),\
for [i=2:size] [hmin(1-e(i)):hmax(1-e(i))] dyn(i) notitle lw 4 lc color(i)

#pause -1

