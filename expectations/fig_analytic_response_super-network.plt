#
set terminal epslatex size 8.7cm,6.0cm standalone color colortext 8 header \
   "\\usepackage{amsmath}\n \
    \\newcommand{\\ft}[0]{\\footnotesize}"
set output "fig_analytic_response_super-network.tex"

set xlabel '$h$'
set ylabel '$\langle a\rangle$'

set format x '$10^{%T}$'
set logscale x

#set key top left Left width 4
#set key spacing 1.5
unset key
set border 3
set xtics nomirror
set ytics nomirror 0, 0.5, 1

set xrange [1e-6:10]
set yrange [0:1.1]

#mean-field solution for N->infty
dt=1
lambda(h)=1-exp(-h*dt)
a(h,m)=lambda(h)/(1-m)
a_cut(h,m)=a(h,m)<1 ? a(h,m):1

standard(h,m)=1 -lambertw(exp(-m)*(1-lambda(h))*(-m))/(-m)
standard_cut(h,m)=standard(h,m)<1 ? standard(h,m):1

#inverse: solve a(h,m)=0.1 -> lamda=(1-m)*0.1 -> h=-ln(1-(1-m)*0.1)/dt
hmin(m)=-log(1-(1-m)*0.1)/dt
hmax(m)=-log(1-(1-m)*0.9)/dt

e="1e0 1e-1 1e-2 1e-3 1e-4"
e(i)=word(e, i)
color(i)=word("1 2 3 4 6",i)
size=words(e)

set sample 1000

## label the curves
#set label 1 sprintf('\ft $m=1-10^{ 0}$') at 9.9e-1,0.8 rotate by 73 center tc ls color(1)
#set label 2 sprintf('\ft $m=1-10^{-1}$') at 5.0e-2,0.8 rotate by 81 center tc ls color(2)
#set label 3 sprintf('\ft $m=1-10^{-2}$') at 5.0e-3,0.8 rotate by 81 center tc ls color(3)
#set label 4 sprintf('\ft $m=1-10^{-3}$') at 5.0e-4,0.8 rotate by 81 center tc ls color(4)
#set label 5 sprintf('\ft $m=1-10^{-4}$') at 5.0e-5,0.8 rotate by 81 center tc ls color(5)
##set label 6 sprintf('\ft $m=1-10^{-5}$') at 5.0e-6,0.8 rotate by 81 center tc ls color(6)
#
#set label 100 "dynamic range" at screen 0.02, graph 1
#
#plot \
#for [i=1:size] a_cut(x,1-e(i)) lw 2 lc color(i),\
#for [i=1:size] [hmin(1-e(i)):hmax(1-e(i))] 1.1 notitle lw 4 lc color(i)

set key top left

plot \
(a_cut(x,1-e(1))+a_cut(x,1-e(2))+a_cut(x,1-e(3))+a_cut(x,1-e(4))+a_cut(x,1-e(5)))/5.0 lw 2 title 'super net',\
standard_cut(x,1.0) lw 2 title 'std net'
#pause -1

