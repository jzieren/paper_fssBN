#
set term epslatex size 8.6cm,6cm standalone color solid 8
set output "fig_analytic_response_fsc_size.tex"

set xlabel '$h$'
set ylabel '$\langle a\rangle$'

set format x '$10^{%T}$'
set logscale x

set key top left Left width 4
set key spacing 1.5

set xrange [1e-6:10]
set yrange [0:1.05]

#mean-field solution for N->infty
dt=1
lambda(h)=1-exp(-h*dt)
a(h,m)=lambda(h)/(1-m)

plot \
lambda(x)/(1-0.923977899317) title '$m=1$, $N=10^1$',\
lambda(x)/(1-0.998738732913) title '$m=1$, $N=10^2$',\
lambda(x)/(1-0.999988099568) title '$m=1$, $N=10^3$'

#pause -1

