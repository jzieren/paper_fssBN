#
set terminal epslatex size 8.7cm,6.0cm standalone color colortext 8 header \
   "\\usepackage{amsmath}\n \
    \\newcommand{\\ft}[0]{\\footnotesize}"
set output "fig_analytic_response_fsc.tex"

set xlabel '$h$'
set ylabel '$\langle a\rangle$'

set format x '$10^{%T}$'
set logscale x

#set key top left Left width 4
#set key spacing 1.5
unset key
set border 3
set xtics nomirror
set ytics nomirror 0, 0.5, 1

set xrange [1e-6:10]
set yrange [0:1.1]

#mean-field solution for N->infty
dt=1
lambda(h)=1-exp(-h*dt)
a(h,m)=lambda(h)/(1-m*(1-lambda(h)))

#inverse: solve a(h,m)=0.1 -> lamda=(1-m)*0.1 -> h=-ln(1-(1-m)*0.1)/dt
hmin(m)=-log(1-(1-m)*0.1)/dt
hmax(m)=-log(1-(1-m)*0.9)/dt

e="1e0 1e-1 1e-2 1e-3 1e-4"
e(i)=word(e, i)
color(i)=word("1 2 3 4 6",i)
size=words(e)

set sample 1000

# label the curves
set label 1 sprintf('\ft $m=1-10^{ 0}$') at 6.0e-1,0.6 rotate by 73 center tc ls color(1)
set label 2 sprintf('\ft $m=1-10^{-1}$') at 8.0e-2,0.6 rotate by 70 center tc ls color(2)
set label 3 sprintf('\ft $m=1-10^{-2}$') at 8.0e-3,0.6 rotate by 70 center tc ls color(3)
set label 4 sprintf('\ft $m=1-10^{-3}$') at 8.0e-4,0.6 rotate by 70 center tc ls color(4)
set label 5 sprintf('\ft $m=1-10^{-4}$') at 8.0e-5,0.6 rotate by 70 center tc ls color(5)
#set label 6 sprintf('\ft $m=1-10^{-5}$') at 5.0e-6,0.8 rotate by 81 center tc ls color(6)

set label 100 "dynamic range" at screen 0.02, graph 1

plot \
for [i=1:size] a(x,1-e(i)) lw 2 lc color(i),\
for [i=1:size] [hmin(1-e(i)):hmax(1-e(i))] 1.1 notitle lw 4 lc color(i)



#pause -1

